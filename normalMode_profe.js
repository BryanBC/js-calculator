function addNumber(value) {
  if (isLimitOperation()) return;

  if (OPERATOR) {
    NUMBER_2 = getNumber(NUMBER_2, value);
  } else {
    NUMBER_1 = getNumber(NUMBER_1, value);
  }

  renderOperation();
}

function addOperator(value) {
  // + - x ÷
  OPERATOR = value;

  const last = NUMBER_1.slice(-1);
  if (last === ".") NUMBER_1 = NUMBER_1.slice(0, -1);

  if (CALC_RESULT) {
    NUMBER_1 = `${CALC_RESULT}`;
    NUMBER_2 = "";
    CALC_RESULT = 0;
  }

  renderOperation();
  renderResult();
}

function calculate() {
  if (!OPERATOR || !NUMBER_2) return;

  const last = NUMBER_2.slice(-1);
  if (last === ".") NUMBER_2 = NUMBER_2.slice(0, -1);

  const operations = {
    "+": (a, b) => a + b,
    "-": (a, b) => a - b,
    x: (a, b) => a * b,
    "÷": (a, b) => a / b,
  };

  CALC_RESULT = operations[OPERATOR](Number(NUMBER_1), Number(NUMBER_2));
  CALC_RESULT = limitDecimals(CALC_RESULT);
  renderOperation();
  renderResult();
}

function deleteLast() {
  if (NUMBER_2) {
    NUMBER_2 = NUMBER_2.slice(0, -1);
  } else if (OPERATOR) {
    OPERATOR = "";
  } else {
    NUMBER_1 = NUMBER_1.slice(0, -1);
    if (NUMBER_1 === "") NUMBER_1 = "0";
  }
  renderOperation();
}
