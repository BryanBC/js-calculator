// ['1','+','55',{number:1,expornente:2,operator:'EXP'}]
// ['0','+','5']

function getLastNumber() {
  const lastPosition = OPERATION_ITEMS.length - 1; // 2
  const lastItem = OPERATION_ITEMS[lastPosition]; // 0 1 (2)
  const lastIsString = typeof lastItem === "string";
  const lastIsObject = typeof lastItem === "object";
  const lastIsNumber = !isNaN(+lastItem);

  return { lastPosition, lastItem, lastIsString, lastIsObject, lastIsNumber };
}

function deleteLastScient() {
  const { lastItem, lastIsNumber } = getLastNumber();

  OPERATION_ITEMS.pop();
  if (lastIsNumber && lastItem.length > 1) {
    OPERATION_ITEMS.push(lastItem.slice(0, -1));
  }

  if (!OPERATION_ITEMS.length) {
    // verificar si no hay nada en el array
    OPERATION_ITEMS.push("0");
  }

  generateOperation();
  renderOperation();
}

function addNumberScient(value) {
  if (isLimitOperation()) return;

  const { lastItem, lastIsNumber, lastPosition } = getLastNumber();

  if (lastIsNumber) {
    OPERATION_ITEMS[lastPosition] = getNumber(lastItem, value);
  } else {
    OPERATION_ITEMS.push(value);
  }

  generateOperation();
  renderOperation();
}

function addOperatorScient(value) {
  if (isLimitOperation()) return;

  const { lastItem, lastIsString, lastPosition } = getLastNumber();

  if (lastIsString && lastItem.slice(-1) === ".") {
    OPERATION_ITEMS[lastPosition] = lastItem.slice(0, -1);
  }
  OPERATION_ITEMS.push(value);

  generateOperation();
  renderOperation();
}

function getPropmt(message, value) {
  while (isNaN(+value) || value === "") {
    value = prompt(message);
    if (isNaN(+value)) {
      alert("Debe ingresar un número válido");
    }
  }

  return value;
}

function addOperatorSpecial(operator) {
  if (isLimitOperation()) return;

  const { lastItem, lastIsString, lastIsNumber, lastIsObject, lastPosition } = getLastNumber();

  if (lastIsString && lastItem.slice(-1) === ".") {
    OPERATION_ITEMS[lastPosition] = lastItem.slice(0, -1);
  }

  if (lastIsNumber || lastIsObject) {
    OPERATION_ITEMS.pop();
  }

  let number = lastIsNumber ? lastItem : lastIsObject ? lastItem.number : "";
  let exponente = "";

  number = getPropmt("Ingrese el número", number);
  exponente = getPropmt("Ingrese el exponente", exponente);

  OPERATION_ITEMS.push({ number, exponente, operator });

  generateOperation();
  renderOperation();
}

function generateOperation() {
  OPERATION = "";

  OPERATION_ITEMS.forEach((item) => {
    const type = typeof item;
    if (type === "object") {
      const { number, exponente, operator } = item;
      if (operator === "EXP") {
        OPERATION += `
          <div class="flex">
            ${number}<p class="up-r">${exponente}</p>
          </div>`;
      } else if (operator === "√") {
        OPERATION += `
          <div class="flex">
            <p class="up-l">${exponente}</p>${operator}${number}
          </div>`;
      }
    } else {
      OPERATION += `<p>${item}</p>`;
    }
  });
}

function isValidParentheses() {
  const opens = OPERATION_ITEMS.filter(item=>item==='(').length
  const closes = OPERATION_ITEMS.filter(item=>item===')').length

  return opens === closes
}

function parseValue(item){
  const type = typeof item;

  if(type === 'object'){
    const {number, exponente, operator} = item;
    if(operator === 'EXP'){
      return number ** exponente
    }else if(operator === '√'){
      return number ** (1/exponente)
    }
  }
  if(item === 'x') return '*'
  if(item === '÷') return '/'
  return item
}

function calculateScient() {

  // ['1','+','55','+',{number:1,expornente:2,operator:'EXP'}, '*']

  if(!isValidParentheses()) return alert('Los parentesis no han sido cerrados correctamente')

  const OPERATION_CALC = []

  OPERATION_ITEMS.forEach(item=>{
    const newValue = parseValue(item)
    OPERATION_CALC.push(newValue)
  })

  const operation = OPERATION_CALC.join(' ')

  try{
    CALC_RESULT = eval(operation)
    CALC_RESULT = limitDecimals(CALC_RESULT)
  }catch(error){
    alert('La operación no es válida')
  }

  // renderOperation()
  renderResult()

}
