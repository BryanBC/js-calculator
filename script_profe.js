let CALC_MODE, CALC_RESULT, NUMBER_1, NUMBER_2, OPERATOR, OPERATION, OPERATION_ITEMS;

function buildButtons() {
  const $buttonsContainer = $("#buttons-container");
  cleanElements($buttonsContainer);

  BUTTONS.forEach((button) => {
    if (!button.modes.includes(CALC_MODE)) return;

    //agregar el boton al contenedor
    const $button = document.createElement("p"); // <p></p>
    $button.textContent = button.value; // <p>8</p>
    $button.classList.add(...button.class); // <p class="basic">8</p>

    // agregar un evento click
    $button.addEventListener("click", () => {
      handleButtonClick(button.value);
    });

    // agregar el boton al contenedor
    $buttonsContainer.appendChild($button);
  });
}

function handleButtonClick(value) {
  const isNormal = CALC_MODE === MODES.normal;
  
  const actions = {
    AC: () => initValues(),
    "←": () => (isNormal ? deleteLast() : deleteLastScient()),
    "=": () => (isNormal ? calculate() : calculateScient()),
  };
  if (actions[value]) return actions[value]();

  const operatorsSpecial = {
    EXP: () => addOperatorSpecial("EXP"),
    "√": () => addOperatorSpecial("√"),
  };
  if (operatorsSpecial[value]) return operatorsSpecial[value]();

  const operators = {
    "+": () => (isNormal ? addOperator("+") : addOperatorScient("+")),
    "-": () => (isNormal ? addOperator("-") : addOperatorScient("-")),
    x: () => (isNormal ? addOperator("x") : addOperatorScient("x")),
    "÷": () => (isNormal ? addOperator("÷") : addOperatorScient("÷")),
    "(": () => addOperatorScient("("),
    ")": () => addOperatorScient(")"),
  };
  if (operators[value]) return operators[value]();

  isNormal ? addNumber(value) : addNumberScient(value);
}

function isLimitOperation() {
  const isNormal = CALC_MODE === MODES.normal;
  if(isNormal){
    return $("#operation").textContent.length > LIMIT_OPERATION;
  }
  return OPERATION_ITEMS.length > LIMIT_OPERATION;
}

function getNumber(variable, value) {
  if (variable.includes(".") && value === ".") return variable;
  if (variable === "0" && value !== ".") return value;
  return variable + value;
}

function limitDecimals(number) {
  return Math.round(number * 10 ** LIMIT_DECIMALS) / 10 ** LIMIT_DECIMALS;
}

function cleanElements($node) {
  while ($node.firstChild) {
    // remueve el primer hijo del nodo
    $node.removeChild($node.firstChild);
  }
}

function initValues() {
  CALC_RESULT = 0; // number
  NUMBER_1 = "0"; // string
  OPERATOR = ""; // string
  NUMBER_2 = ""; // string

  OPERATION = "<p>0</p>";
  OPERATION_ITEMS = ["0"];

  renderOperation();
  renderResult();
}

function renderMode() {
  $("#mode").textContent = CALC_MODE;
}
function renderOperation() {
  const isNormal = CALC_MODE === MODES.normal;

  if (isNormal) {
    $("#operation").textContent = NUMBER_1 + OPERATOR + NUMBER_2;
  } else {
    $("#operation").innerHTML = OPERATION;
  }
}
function renderResult() {
  $("#result").textContent = "=" + CALC_RESULT;
}

function changeMode() {
  CALC_MODE = CALC_MODE === MODES.normal ? MODES.scient : MODES.normal;
  buildCalculator();
}

function buildCalculator() {
  initValues();
  renderMode();
  buildButtons();
}

function handleKeyboard(e) {
  const KEYBOARD = {
    8: () => handleButtonClick("←"), // Backspace
    13: () => handleButtonClick("="), // Enter
    27: () => handleButtonClick("AC"), // Escape

    // NUMBERS - LETTERS PAD
    48: () => handleButtonClick("0"),
    49: () => handleButtonClick("1"),
    50: () => handleButtonClick("2"),
    51: () => handleButtonClick("3"),
    52: () => handleButtonClick("4"),
    53: () => handleButtonClick("5"),
    54: () => handleButtonClick("6"),
    55: () => handleButtonClick("7"),
    56: () => handleButtonClick("8"),
    57: () => handleButtonClick("9"),

    // NUMBERS - NUMPAD
    96: () => handleButtonClick("0"),
    97: () => handleButtonClick("1"),
    98: () => handleButtonClick("2"),
    99: () => handleButtonClick("3"),
    100: () => handleButtonClick("4"),
    101: () => handleButtonClick("5"),
    102: () => handleButtonClick("6"),
    103: () => handleButtonClick("7"),
    104: () => handleButtonClick("8"),
    105: () => handleButtonClick("9"),

    // OPERATORS - LETTERS PAD
    187: () => handleButtonClick("+"),
    189: () => handleButtonClick("-"),
    191: () => handleButtonClick("÷"),
    88: () => handleButtonClick("x"),
    190: () => handleButtonClick("."),

    // OPERATORS - NUMPAD
    107: () => handleButtonClick("+"),
    109: () => handleButtonClick("-"),
    111: () => handleButtonClick("÷"),
    106: () => handleButtonClick("x"),
    110: () => handleButtonClick("."),
  };

  if (KEYBOARD[e.keyCode]) KEYBOARD[e.keyCode]();
}

function init() {
  CALC_MODE = MODES.normal;
  buildCalculator();
  document.addEventListener("keydown", handleKeyboard);
}

init();
