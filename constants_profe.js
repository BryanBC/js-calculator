const MODES = {
  normal: "Normal",
  scient: "Científica",
};

const BUTTONS = [
  { value: "AC", modes: [MODES.scient], class: ["clear"] },
  { value: "(", modes: [MODES.scient], class: ["basic"] },
  { value: ")", modes: [MODES.scient], class: ["basic"] },
  { value: "←", modes: [MODES.scient], class: ["operator", "col-2"] },
  { value: "7", modes: [MODES.normal, MODES.scient], class: ["basic"] },
  { value: "8", modes: [MODES.normal, MODES.scient], class: ["basic"] },
  { value: "9", modes: [MODES.normal, MODES.scient], class: ["basic"] },
  { value: "EXP", modes: [MODES.scient], class: ["operator", "text-sm"] },
  { value: "√", modes: [MODES.scient], class: ["operator"] },
  { value: "AC", modes: [MODES.normal], class: ["clear"] },
  { value: "←", modes: [MODES.normal], class: ["operator"] },
  { value: "4", modes: [MODES.normal, MODES.scient], class: ["basic"] },
  { value: "5", modes: [MODES.normal, MODES.scient], class: ["basic"] },
  { value: "6", modes: [MODES.normal, MODES.scient], class: ["basic"] },
  { value: "x", modes: [MODES.normal, MODES.scient], class: ["operator"] },
  { value: "÷", modes: [MODES.normal, MODES.scient], class: ["operator"] },
  { value: "1", modes: [MODES.normal, MODES.scient], class: ["basic"] },
  { value: "2", modes: [MODES.normal, MODES.scient], class: ["basic"] },
  { value: "3", modes: [MODES.normal, MODES.scient], class: ["basic"] },
  { value: "+", modes: [MODES.normal, MODES.scient], class: ["operator"] },
  { value: "-", modes: [MODES.normal, MODES.scient], class: ["operator"] },
  { value: "0", modes: [MODES.normal, MODES.scient], class: ["basic", "col-2"] },
  { value: ".", modes: [MODES.normal, MODES.scient], class: ["basic"] },
  { value: "=", modes: [MODES.normal, MODES.scient], class: ["equal", "col-2"] },
];

const LIMIT_OPERATION = 18;
const LIMIT_DECIMALS = 8;
